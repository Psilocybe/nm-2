#ifndef _CONFIGURATION_READER_H
#define _CONFIGURATION_READER_H

#include <map>
#include <sstream>
#include <stdexcept>
#include <string>

struct Parameter
{
    std::string value;
    template<typename T> operator T() const
    {
       std::stringstream ss(value);
       T convertedValue;
       if (ss >> convertedValue) return convertedValue;
       else throw std::runtime_error("Conversion failed");
    }
};

class ConfigurationReader
{
  char commentCharacter;
  std::map<std::string, std::string> options;

public:
  ConfigurationReader(const char * configFileName, char comment);

  Parameter lookupParameter(std::string key);
};

#endif
