#ifndef _HEAT_TRANSFER_EQUATION_H
#define _HEAT_TRANSFER_EQUATION_H

#include "Configuration.h"

class HeatTransferEquation
{
	int gridSize;
	double D;
	double xMin;
	double xMax;
	double initTx0;
	double initT0t;
	double initT1t;
	int analyticalPrecision;
	
	double * numericalValues;
	double * analyticalValues;
	double * currentStep;
	double * normVector;

	int index(int t, int x) const;
public:
	HeatTransferEquation(Configuration &configuration);
	~HeatTransferEquation();
	
	void calculateExplicit(double r);
	double getAvgNorm() const;
	void getAnalyticalValues(double * dest) const;
	void getNumericalValues(double * dest) const;
};

#endif
