#include "HeatTransferEquation.h"

int HeatTransferEquation::index(int t, int x) const
{
	return (gridSize * t) + x;
}

HeatTransferEquation::HeatTransferEquation(Configuration &configuration)
{
	gridSize = configuration.gridSize + 1;
	D = configuration.D;
	xMin = configuration.xMin;
	xMax = configuration.xMax;
	initTx0 = configuration.initTx0;
	initT0t = configuration.initT0t;
	initT1t = configuration.initT1t;
	analyticalPrecision = configuration.analyticalPrecision;
	currentStep = new double[gridSize];
	numericalValues = new double[3 * gridSize];
	analyticalValues = new double[3 * gridSize];
	normVector = new double[3];

	for(int i = 0; i < gridSize - 1; i++) currentStep[i] = 0;
	currentStep[gridSize - 1] = 1;
}

HeatTransferEquation::~HeatTransferEquation()
{
	delete[] numericalValues;
	delete[] analyticalValues;
	delete[] normVector;
	delete[] currentStep;
}

void HeatTransferEquation::calculateExplicit(double r)
{
	double * previousStep = new double[gridSize];
	
	double timeStops[] = { 1.2, 5.2, 10.0 };
	double time = 0.0;
	for (int ts = 0; ts < 3; ts++)
	{
		while (time <= timeStops[ts])
		{
			time += r;
			std::copy(currentStep, currentStep + gridSize, previousStep);
			for (int g = 1; g < gridSize - 1; g++)
			{
				double f_i = previousStep[g];
				double f_i_plus_1 = previousStep[g + 1];
				double f_i_minus_1 = previousStep[g - 1];
				currentStep[g] = r * (f_i_plus_1 + f_i_minus_1) + (1 - 2*r) * f_i;
			}
		}
		std::copy(currentStep, currentStep + gridSize,
			&numericalValues[index(ts, 0)]);
		//calculateError(time, ts);
	}
	
	delete[] previousStep;
}

double HeatTransferEquation::getAvgNorm() const
{
	double sum = 0.0;
	for (int i = 0; i < 3; i++)
		sum += normVector[i];
	return sum / 3;
}

void HeatTransferEquation::getAnalyticalValues(double * dest) const
{
	std::copy(
		analyticalValues,
		analyticalValues + 3 * gridSize,
		dest);
}

void HeatTransferEquation::getNumericalValues(double * dest) const
{
	std::copy(
		numericalValues,
		numericalValues + 3 * gridSize,
		dest);
}
