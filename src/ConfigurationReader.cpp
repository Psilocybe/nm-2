#include "ConfigurationReader.h"

#include <fstream>

ConfigurationReader::ConfigurationReader(
  const char * configFileName, char commentCharacter)
{
    this->commentCharacter = commentCharacter;
    std::ifstream config(configFileName);
    std::string line;
    while(std::getline(config, line))
    {
      if (line[0] == commentCharacter) continue; //skip comments
      std::istringstream is_line(line);
      std::string key;
      if(std::getline(is_line, key, '='))
      {
        std::string value;
        if (std::getline(is_line, value))
        {
          options[key] = value;
        }
      }
    }
    config.close();
}

Parameter ConfigurationReader::lookupParameter(std::string key)
{
      std::string value = options[key];
      return { value };
}

