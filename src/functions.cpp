#include "functions.h"

#include <cmath>

#define PI 3.1415

double heat(double x, double t, double D, int accuraccy)
{
	double sum = 0.0;
	double expConst = -D * PI;
	for(int n = 1; n <= accuraccy; n++)
	{
		double nPI = n * PI;
		double term = (1.0 / (nPI)) * exp(expConst * nPI * n * t) * sin (nPI * x);
		if (n % 2 == 0) sum += term;
		else sum -= term;
	}
	return (x + 2 * sum);
}

