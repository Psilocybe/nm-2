#ifndef _CONFIGURATION_H
#define _CONFIGURATION_H

#include <string>

struct Configuration
{
	int gridSize;
	double D;
	double xMin;
	double xMax;	
	double initTx0;
	double initT0t;
	double initT1t;
	int analyticalPrecision;
};

#endif

