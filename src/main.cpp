#include "functions.h"
#include "Configuration.h"
#include "HeatTransferEquation.h"
#include "ConfigurationReader.h"

#include <iostream>
#include <iomanip>
#include <iterator>
#include <fstream>

void readConfig(const char * fileName, Configuration &config);

int main(int argc, char* argv[])
{
	/*if (argc == 1)
	{
		std::cout << "Specify config file." << std::endl;
		return -1;
	}

	// iterate through all configuration files
	for (int i = 1; i < argc; i++)
	{
		Configuration configuration;
		readConfig(argv[i], configuration);
		HeatTransferEquation equation(configuration);
				
	}
	*/
	
	double r = 0.6;	
	Configuration configuration;
	readConfig(argv[1], configuration);
	HeatTransferEquation equation(configuration);
	equation.calculateExplicit(r);
	std::cout << "Norm: " << equation.getAvgNorm() << std::endl;
	std::string fName = "result" + std::to_string(static_cast<long double>(r)) + ".csv";
	
	double * asdf = new double[3 * (configuration.gridSize + 1)];
	equation.getNumericalValues(asdf);
	std::ofstream result(fName);
	for(int t = 0; t < 3; t++)
	{
		for(int i = 0; i <= configuration.gridSize; i++)
			result << asdf[t * (configuration.gridSize + 1) + i] << ";";
		result << std::endl;
	}
	result.close();
	delete[] asdf;
	return 0;
}

void readConfig(const char * fileName, Configuration &config)
{
	ConfigurationReader reader(fileName, '#');
	config.gridSize = reader.lookupParameter("gridSize");
	config.D = reader.lookupParameter("D");
	config.xMin = reader.lookupParameter("xMin");
	config.xMax = reader.lookupParameter("xMax");
	config.initTx0 = reader.lookupParameter("initTx0");
	config.initT0t = reader.lookupParameter("initT0t");
	config.initT1t = reader.lookupParameter("initT1t");
	config.analyticalPrecision = reader.lookupParameter("analyticalPrecision");
}

